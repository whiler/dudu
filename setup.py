#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

from setuptools import setup

setup(
    name='dudu',
    version='0.0.0',
    author='whiler',
    author_email='wenwu500@qq.com',
    install_requires=[
        'aioquic',
    ],
    packages=['dudu', 'dudu/net'],
    python_requires='>=3.6, <4',
    entry_points={
        'console_scripts': [
            'dudu=dudu.__main__:main',
        ],
    },
    classifiers=[
        'Environment :: Console',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: Microsoft :: Windows',
        'Operating System :: POSIX',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
    ]
)
