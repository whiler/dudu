#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
from __future__ import absolute_import

import logging
import asyncio

import dudu.tap
import dudu.utils

logger = logging.getLogger(__name__)


class Selector(object):
    scheme = 'udp'

    def filter(self, data):
        return True


class Factory(object):
    def __init__(self, dispatcher, loop, interval=293.0):
        self.loop = loop
        self.interval = interval
        self._protocol = Protocol(dispatcher)
        self._protocols = dict()

    def protocol(self):
        return self._protocol

    def dial(self, transport, remote):
        sockname = transport.get_extra_info('sockname')
        key = (sockname, remote)
        if key not in self._protocols:
            self._protocols[key] = Protocol(None)
            self._protocols[key].connection_made(transport)
        return self._protocols[key]


class Protocol(asyncio.DatagramProtocol):
    def __init__(self, handler, *args, **kwargs):
        super(Protocol, self).__init__(*args, **kwargs)
        self.handler = handler
        self.transport = None
        self._writeable = True

    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        host, port, *_ = addr
        frame = dudu.tap.Frame(data)
        self.handler(frame, (Selector.scheme, host, port), self)

    def writeto(self, frame, remote):
        scheme, host, port = remote
        assert Selector.scheme == scheme, 'invalid scheme'
        return self.transport.sendto(frame, addr=(host, port))

    def writeable(self):
        return self._writeable

    def __str__(self):
        host, port, *_ = self.transport.get_extra_info('sockname')
        return dudu.utils.fmthostport(Selector.scheme, host, port)
