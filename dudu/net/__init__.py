#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
from __future__ import absolute_import

import asyncio
import logging
import socket

import dudu.tap
import dudu.utils

logger = logging.getLogger(__name__)

GLOBALv4 = '8.8.8.8'
GLOBALv6 = '2001:4860:4860::8888'
ANYV4 = '0.0.0.0'
ANYV6 = '::'


class Server(object):
    def __init__(self, listens, loop):
        self.listens = listens
        self.loop = loop
        self.bound = dict()
        self.supported = dict()

    def register(self, selector, factory):
        self.supported[selector.scheme] = (selector, factory)
        return self

    async def start_serving(self):
        for scheme, hostname, port in self.listens:
            if scheme not in self.supported:
                raise ValueError('unsupported scheme')
            selector, factory = self.supported[scheme]
            for af, tpe, proto, canon, sockaddr in await self.loop.getaddrinfo(hostname or None, port,
                                                                               family=socket.AF_UNSPEC,
                                                                               type=socket.SOCK_DGRAM,
                                                                               proto=socket.IPPROTO_UDP,
                                                                               flags=socket.AI_PASSIVE):
                local_addr = sockaddr[0:2]
                if local_addr not in self.bound:
                    self.bound[local_addr] = await self.loop.create_datagram_endpoint(MultiplexProtocol,
                                                                                      local_addr=local_addr,
                                                                                      reuse_port=True)
                transport, protocol = self.bound[local_addr]
                protocol.register(selector, factory)

    def close(self):
        for transport, protocol in self.bound.values():
            if hasattr(protocol, 'close') and callable(getattr(protocol, 'close')):
                protocol.close()
            transport.close()

    def address(self):
        for transport, protocol in self.bound.values():
            sock = transport.get_extra_info('socket')
            if sock is None:
                continue
            sockaddr = list(sock.getsockname())
            with socket.socket(sock.family, sock.type, sock.proto) as shadow:
                port = sockaddr[1]
                sockaddr[1] = 0
                shadow.bind(tuple(sockaddr))
                if socket.AF_INET == shadow.family:
                    host = GLOBALv4
                elif socket.AF_INET6 == shadow.family:
                    host = GLOBALv6
                else:
                    continue
                shadow.connect((host, 53))
                host, *_ = shadow.getsockname()
            for selector, factory in protocol.registered:
                yield dudu.utils.fmthostport(selector.scheme, host, port)

    def dialer(self):
        return Dialer(self.bound)


class MultiplexProtocol(asyncio.DatagramProtocol):
    def __init__(self, *args, **kwargs):
        super(MultiplexProtocol, self).__init__(*args, **kwargs)
        self.registered = list()
        self.transport = None

    def register(self, selector, factory):
        self.registered.append((selector, factory))
        return self

    def connection_made(self, transport):
        self.transport = transport

    def connection_lost(self, exc):
        super(MultiplexProtocol, self).connection_lost(exc)
        if exc is not None:
            logger.error(exc)

    def datagram_received(self, data, addr):
        for selector, factory in self.registered:
            if selector.filter(data):
                protocol = factory.protocol()
                protocol.connection_made(self.transport)
                protocol.datagram_received(data, addr)
                break

    def error_received(self, exc):
        super(MultiplexProtocol, self).error_received(exc)
        logger.error(exc)


class Dialer(object):
    def __init__(self, bound):
        self.bound = bound

    def dial(self, hostport):
        scheme, host, port = hostport
        if -1 != host.find(':'):  # IPv6
            for (vhost, vport), (transport, protocol) in self.bound.items():
                if -1 == vhost.find(':'):
                    continue
                elif host != vhost and ANYV6 != vhost:
                    continue
                for selector, factory in protocol.registered:
                    if scheme != selector.scheme:
                        continue
                    return factory.dial(transport, (host, port))
        else:  # IPv4
            for (vhost, vport), (transport, protocol) in self.bound.items():
                if -1 != vhost.find(':'):
                    continue
                elif host != vhost and ANYV4 != vhost:
                    continue
                for selector, factory in protocol.registered:
                    if scheme != selector.scheme:
                        continue
                    return factory.dial(transport, (host, port))
        raise ValueError('unreachable address')
