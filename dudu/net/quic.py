#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
from __future__ import absolute_import

import logging
import asyncio

import aioquic.asyncio.protocol
import aioquic.asyncio.server
import aioquic.quic.configuration
import aioquic.quic.events
import aioquic.quic.connection

import dudu.tap
import dudu.utils

logger = logging.getLogger(__name__)

ID_LENGTH = 8
IDLE_TIMEOUT = 300.0


class Selector(object):
    scheme = 'quic'

    def filter(self, data):
        buf = aioquic.buffer.Buffer(data=data)
        try:
            aioquic.quic.packet.pull_quic_header(buf, host_cid_length=ID_LENGTH)
        except ValueError:
            return False
        return True


class Factory(object):
    def __init__(self, dispatcher, loop, capath, certpath, keypath, password=None):
        self.dispatcher = dispatcher
        self.loop = loop

        self.srv = aioquic.quic.configuration.QuicConfiguration(
            is_client=False,
            connection_id_length=ID_LENGTH,
            idle_timeout=IDLE_TIMEOUT,
        )
        self.srv.load_cert_chain(certpath, keypath, password=password)
        self.srv.load_verify_locations(capath)

        self.cli = aioquic.quic.configuration.QuicConfiguration(
            is_client=True,
            connection_id_length=ID_LENGTH,
            idle_timeout=IDLE_TIMEOUT,
        )
        self.cli.load_cert_chain(certpath, keypath, password=password)
        self.cli.load_verify_locations(capath)

        self._protocol = QuicServer(configuration=self.srv, create_protocol=self.create_protocol)

    def create_protocol(self, *args, **kwargs):
        return Protocol(self.dispatcher, *args, **kwargs)

    def protocol(self):
        return self._protocol

    def dial(self, transport, remote):
        sockname = transport.get_extra_info('sockname')
        key = (sockname, remote)
        conn = aioquic.quic.connection.QuicConnection(configuration=self.cli)
        protocol = Protocol(self.dispatcher, quic=conn)
        protocol.connection_made(transport)
        protocol.connect(remote)
        self._protocol.set(key, protocol)
        return protocol


class QuicServer(aioquic.asyncio.server.QuicServer):
    def __init__(self, *args, **kwargs):
        super(QuicServer, self).__init__(*args, **kwargs)
        self.registered = dict()

    def set(self, key, protocol):
        self.registered[key] = protocol
        return self

    def get(self, key):
        return self.registered.get(key)

    def has(self, key):
        return key in self.registered

    def dispatch(self, data, addr, header):
        dst = header.destination_cid
        for protocol in self.registered.values():
            for conn in protocol._quic._host_cids:
                if dst == conn.cid:
                    return protocol
        return None

    def datagram_received(self, data, addr):
        buf = aioquic.buffer.Buffer(data=data)
        header = aioquic.quic.packet.pull_quic_header(buf, host_cid_length=self._configuration.connection_id_length)

        # version negotiation
        if (
            header.version is not None
            and header.version not in self._configuration.supported_versions
        ):
            self._transport.sendto(
                aioquic.quic.packet.encode_quic_version_negotiation(
                    source_cid=header.destination_cid,
                    destination_cid=header.source_cid,
                    supported_versions=self._configuration.supported_versions,
                ),
                addr,
            )
            return

        # registered protocols
        protocol = self.dispatch(data, addr, header)
        if protocol is not None:
            protocol.datagram_received(data, addr)
            return

        super(QuicServer, self).datagram_received(data, addr)


class Protocol(aioquic.asyncio.protocol.QuicConnectionProtocol):
    def __init__(self, callback, *args, **kwargs):
        super(Protocol, self).__init__(*args, **kwargs)
        self.callback = callback
        self.remote = None
        self.stream_id = None

    def datagram_received(self, data, addr):
        super(Protocol, self).datagram_received(data, addr)
        self.remote = addr

    def quic_event_received(self, event):
        if isinstance(event, aioquic.quic.events.StreamDataReceived):
            if dudu.tap.Frame.MINSIZE <= len(event.data):
                host, port, *_ = self.remote
                frame = dudu.tap.Frame(event.data)
                self.callback(frame, (Selector.scheme, host, port), self)

    def writeto(self, frame, port):
        if self.stream_id is None:
            self.stream_id = self._quic.get_next_available_stream_id()
        self._quic.send_stream_data(self.stream_id, frame, False)
        self.transmit()

    def writeable(self):
        return True

    def __str__(self):
        host, port, *_ = self._transport.get_extra_info('sockname')
        return dudu.utils.fmthostport(Selector.scheme, host, port)
