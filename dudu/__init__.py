#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
from __future__ import absolute_import

import logging
import os

import dudu.dynamic
import dudu.interact
import dudu.net
import dudu.net.plain
import dudu.net.quic
import dudu.tap
import dudu.utils

logger = logging.getLogger(__name__)


class Configure(object):
    def __init__(self, verbose, interact,
                 taps, tuns, listens, ca, cert, key, passwd):
        self.verbose = verbose
        self.interact = interact
        self.taps = taps
        self.tuns = tuns
        self.listens = listens
        self.ca = ca
        self.cert = cert
        self.key = key
        self.passwd = passwd


class Dudu(object):
    def __init__(self, cfg):
        self.cfg = cfg
        self.loop = None
        self.servers = list()
        self.tap_switch = None

    def __enter__(self):
        return self

    def __exit__(self, tpe, value, traceback):
        self.destory()

    async def arun(self, loop):
        logging.basicConfig(level=dudu.utils.loglevel(self.cfg.verbose),
                            format='%(asctime)s %(levelname)s %(name)s %(message)s',
                            datefmt='%FT%T%z')
        self.infocfg(self.cfg)
        self.loop = loop

        self.tap_switch = dudu.tap.Switch()
        self.tap_switch.start_refresh(loop)
        self.servers.append(self.tap_switch)

        tap = dudu.tap.Server(self.cfg.taps, self.handle_tap_dev, loop)
        tap.start_serving()
        self.servers.append(tap)

        net = dudu.net.Server(self.cfg.listens, loop)
        net.register(dudu.net.plain.Selector(), dudu.net.plain.Factory(self.handle_tap_net, loop))
        net.register(dudu.net.quic.Selector(), dudu.net.quic.Factory(self.handle_tap_net, loop,
                                                                     self.cfg.ca, self.cfg.cert, self.cfg.key, self.cfg.passwd))
        await net.start_serving()
        self.servers.append(net)
        self.tap_switch.set_dialer(net.dialer())

        dynamic = dudu.dynamic.Server(loop)
        dynamic.announcer(tap.macs, net.address)
        dynamic.start_serving()
        self.servers.append(dynamic)
        self.tap_switch.set_dynamic(dynamic)


        scheme, host, port = self.cfg.interact
        assert 'tcp' == scheme, 'invalid scheme'
        server = await loop.create_server(lambda: dudu.interact.Protocol(self.handle_interact), host, port,
                                          reuse_address=True, reuse_port=True)
        await server.start_serving()
        self.servers.append(server)

    def destory(self):
        for server in self.servers:
            server.close()
        return True

    def infocfg(self, cfg):
        logger.info('verbose: %d', cfg.verbose)
        logger.info('interact: %s', dudu.utils.fmthostport(*cfg.interact))
        if cfg.taps:
            logger.info('TAP devices:')
            for tap in cfg.taps:
                logger.info('- %s', tap)
        if cfg.tuns:
            logger.info('TUN devices:')
            for tun in cfg.tuns:
                logger.info('- %s', tun)
        if cfg.listens:
            logger.info('listens:')
            for hostport in cfg.listens:
                logger.info('- %s', dudu.utils.fmthostport(*hostport))
        if cfg.ca:
            logger.info('ca: %s', cfg.ca)
        if cfg.cert:
            logger.info('cert: %s', cfg.cert)
        if cfg.key:
            logger.info('key: %s', cfg.key)
        if cfg.passwd:
            logger.info('passwd: ******')

    def handle_interact(self, msg):
        logger.debug('received interact message: %s', msg)
        mem = list()
        if msg.startswith('tap '):
            if 'tap list' == msg:
                for mac, port, receiver in self.tap_switch.list():
                    if isinstance(port, tuple):
                        mem.append('{} on {} via {}'.format(dudu.tap.fmtmac(mac), dudu.utils.fmthostport(*port), receiver))
                    else:
                        mem.append('{} on {} via {}'.format(dudu.tap.fmtmac(mac), port, receiver))
            elif msg.startswith('tap set '):
                *_, mac, port = msg.split()
                hostport = dudu.utils.parsehostport(port)
                ret = self.tap_switch.set(dudu.tap.parsemac(mac), hostport)
                mem.append(str(ret))
            elif msg.startswith('tap remove '):
                *_, mac = msg.split()
                ret = self.tap_switch.remove(dudu.tap.parsemac(mac))
                mem.append(str(ret))
        mem.append('')
        return os.linesep.join(mem)

    def handle_tap_dev(self, frame, ifname, writer):
        logger.debug('read frame(%s) from %s', frame, ifname)
        self.tap_switch.forward(frame, ifname, writer)

    def handle_tap_net(self, frame, remote, sender):
        logger.debug('received frame(%s) from %s', frame, dudu.utils.fmthostport(*remote))
        self.tap_switch.forward(frame, remote, sender)
