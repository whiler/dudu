#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
import asyncio
import logging
import platform
import selectors
import signal

logger = logging.getLogger(__name__)


class AMainLoop(object):
    def __init__(self):
        self.loop = None
        self.quit = None

    def __enter__(self):
        if 'Darwin' == platform.system():
            selector = selectors.SelectSelector()
            asyncio.set_event_loop(asyncio.SelectorEventLoop(selector))
        self.loop = asyncio.get_event_loop()
        self.quit = asyncio.Event()
        for sig in (signal.SIGINT, signal.SIGTERM):
            self.loop.add_signal_handler(sig, self.quit.set)
        return self

    def wait(self):
        return self.loop.run_until_complete(self.quit.wait())

    def run(self, coro):
        return self.loop.create_task(coro)

    def __exit__(self, tpe, value, traceback):
        for task in asyncio.Task.all_tasks():
            task.cancel()
        self.loop.run_until_complete(self.loop.shutdown_asyncgens())
        self.loop.stop()
        self.loop.close()
