#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
from __future__ import absolute_import

import asyncio
import logging

logger = logging.getLogger(__name__)


class Server(object):
    def __init__(self, loop, interval=300):
        self.loop = loop
        self.announcers = list()
        self.interval = interval * 1.009
        self.announcing = False
        self.task = None
        self.announced = set()

    def start_serving(self):
        self.announcing = True
        coro = self.start_announce()
        self.task = self.loop.create_task(coro)

    async def start_announce(self):
        while self.announcing:
            for kgen, vgen in self.announcers:
                values = list(vgen())
                for key in kgen():
                    self.announced.add(key)
                    logger.debug('announcing: %s = %r', key, values)
            await asyncio.sleep(self.interval)

    def close(self):
        self.announcing = False
        if self.task is not None:
            for key in self.announced:
                logger.debug('clean announced: %s', key)
            self.task.cancel()

    def announcer(self, kgen, vgen):
        self.announcers.append((kgen, vgen))
        return self

    def list(self):
        return ()

    def has(self, key):
        return False

    def get(self, key):
        pass
