#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
import logging
import urllib.parse

logger = logging.getLogger(__name__)


def loglevel(v):
    if 0 >= v:
        return logging.WARNING
    elif 1 == v:
        return logging.INFO
    elif 2 == v:
        return logging.DEBUG
    return logging.NOTSET


def parsehostport(s):
    parsed = urllib.parse.urlparse(s)
    if not parsed.scheme or not parsed.netloc or not isinstance(parsed.port, int) or 1024 >= parsed.port or 65535 < parsed.port:
        raise ValueError('invalid')
    return parsed.scheme, (parsed.hostname or '').strip('[]'), parsed.port


def fmthostport(scheme, hostname, port):
    if -1 != hostname.find(':'):
        return '{}://[{}]:{}'.format(scheme, hostname, port)
    return '{}://{}:{}'.format(scheme, hostname, port)
