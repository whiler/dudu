#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
from __future__ import absolute_import

import asyncio
import logging

logger = logging.getLogger(__name__)


class Protocol(asyncio.Protocol):
    def __init__(self, callback, *args, **kwargs):
        super(Protocol, self).__init__(*args, **kwargs)
        self.callback = callback
        self.transport = None

    def connection_made(self, transport):
        self.transport = transport

    def data_received(self, binary):
        resp = self.callback(binary.decode().strip())
        self.transport.write(resp.lstrip().encode())

    def connection_lost(self, exc):
        self.transport.close()
