#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
from __future__ import absolute_import

import argparse

import dudu
import dudu.mainloop
import dudu.utils


def main():
    parser = argparse.ArgumentParser(prog='dudu')
    parser.add_argument('--tap', '-2',
                        type=str,
                        nargs='+',
                        default=list(),
                        help='TAP device name(s)')
    parser.add_argument('--tun', '-3',
                        type=str,
                        nargs='+',
                        default=list(),
                        help='TUN device name(s)')
    parser.add_argument('--interact', '-i',
                        type=dudu.utils.parsehostport,
                        default='tcp://localhost:30052',
                        help='listen and interact address')
    parser.add_argument('--listen', '-l',
                        type=dudu.utils.parsehostport,
                        nargs='+',
                        default=[('udp', '', 25717)],
                        help='listen and serve address(es)')
    parser.add_argument('--ca',
                        type=str,
                        help='TLS CA certificates')
    parser.add_argument('--cert',
                        type=str,
                        help='TLS certificate')
    parser.add_argument('--key',
                        type=str,
                        help='TLS certificate private key')
    parser.add_argument('--passwd',
                        type=str,
                        help='TLS certificate private key password')
    parser.add_argument('--verbose', '-v',
                        default=0,
                        help='verbose',
                        action='count')
    args = parser.parse_args()

    cfg = dudu.Configure(args.verbose, args.interact,
                         args.tap, args.tun, args.listen,
                         args.ca, args.cert, args.key, args.passwd)
    with dudu.mainloop.AMainLoop() as mainLoop:
        with dudu.Dudu(cfg) as ins:
            mainLoop.run(ins.arun(mainLoop.loop))
            mainLoop.wait()
    return 0


if '__main__' == __name__:
    import sys
    sys.exit(main())
