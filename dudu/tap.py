#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
from __future__ import absolute_import

import binascii
import fcntl
import logging
import os
import platform
import re
import struct
import subprocess

import dudu.utils

logger = logging.getLogger(__name__)


class Server(object):
    def __init__(self, taps, handler, loop):
        self.devs = list(map(Device, taps))
        self.handler = handler
        self.loop = loop
        self.writers = dict()

    def start_serving(self):
        for dev in self.devs:
            dev.up()
            self.writers[dev.fd] = Writer(dev.fd, dev.name)
            self.loop.add_reader(dev.fd, self.read, dev.fd, dev.name)

    def read(self, fd, ifname):
        binary = os.read(fd, 4096)
        writer = self.writers[fd]
        return self.handler(Frame(binary), ifname, writer)

    def close(self):
        for dev in self.devs:
            self.loop.remove_reader(dev.fd)
            dev.down()
            dev.close()

    def macs(self):
        for dev in self.devs:
            yield dev.mac


class Device(object):
    def __init__(self, ifname):
        fd, name = tap(ifname)
        self.fd = fd
        self.name = name

    def fileno(self):
        return self.fd

    def up(self):
        self.ifconfig(self.name, 'up')
        return True

    def down(self):
        self.ifconfig(self.name, 'down')
        return True

    @property
    def mac(self):
        content = self.ifconfig(self.name)
        match = re.search(r'^\s+ether\s([0-9a-fA-F]{2}(:[0-9a-fA-F]{2}){5})',
                          content,
                          re.M)
        s, _ = match.groups()
        return s

    @mac.setter
    def mac(self, s):
        self.ifconfig(self.name, 'ether', s)
        return True

    def ifconfig(self, *args):
        cmd = ['ifconfig']
        cmd.extend(args)
        ret = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if 0 != ret.returncode:
            raise BrokenPipeError(ret.stderr.decode())
        return ret.stdout.decode()

    def close(self):
        os.close(self.fd)


class Frame(bytes):
    STRUCT = struct.Struct('>6s6s')
    MINSIZE = 12

    def __init__(self, *args, **kwargs):
        self.dst, self.src = self.STRUCT.unpack(self[0:self.STRUCT.size])

    def __str__(self):
        return '{} <- {}'.format(fmtmac(self.dst), fmtmac(self.src))


def fmtmac(mac):
    return ':'.join(map('{:02x}'.format, mac))


def parsemac(s):
    return binascii.unhexlify(s.replace(':', ''))


class Switch(object):
    BROADCAST = struct.pack('>6B', 0xff, 0xff, 0xff, 0xff, 0xff, 0xff)
    IPv4_MULTICAST_PREFIX = struct.pack('>3B', 0x1, 0x0, 0x5e)
    IPv6_MULTICAST_PREFIX = struct.pack('>H', 0x3333)
    MULTICAST_PREFIXES = (IPv4_MULTICAST_PREFIX, IPv6_MULTICAST_PREFIX)

    def __init__(self, dynamic=None, dialer=None):
        self.writers = dict()  # port -> writer
        self.ports = dict()  # mac -> port
        self.dynamic = dynamic
        self.dialer = dialer
        self.timer = None

    def set_dynamic(self, dynamic):
        self.dynamic = dynamic

    def set_dialer(self, dialer):
        self.dialer = dialer

    def forward(self, frame, port, writer):
        self.set(frame.src, port, writer)
        for port in self.iroute(frame.dst, port):
            if isinstance(port, tuple):
                logger.debug('forward %s to %s', frame, dudu.utils.fmthostport(*port))
            else:
                logger.debug('forward %s to %s', frame, port)
            writer = self.writers.get(port)
            if writer is None and self.dialer is not None:
                writer = self.dialer.dial(port)
                self.writers[port] = writer
            writer.writeto(frame, port)

    def iroute(self, dst, port):
        if self.ismulticast(dst) or self.isbroadcast(dst):
            uniq = set()
            for v in self.ports.values():
                if port == v or v in uniq:
                    continue
                uniq.add(v)
                yield v
            if self.dynamic is not None:
                for mac, v in self.dynamic.list():
                    if port == v or v in uniq:
                        continue
                    uniq.add(v)
                    yield v
        elif dst in self.ports:
            yield self.ports[dst]
        elif self.dynamic is not None and self.dynamic.has(dst):
            yield self.dynamic.get(dst)

    def ismulticast(self, mac):
        return mac.startswith(self.MULTICAST_PREFIXES)

    def isbroadcast(self, mac):
        return self.BROADCAST == mac

    def list(self):
        for mac, port in self.ports.items():
            yield mac, port, self.writers.get(port, 'uninitiated')
        if self.dynamic is not None:
            for mac, port in self.dynamic.list():
                yield mac, port, 'dynamic'

    def set(self, mac, port, writer=None):
        if writer is not None and port not in self.writers:
            self.writers[port] = writer
        if mac not in self.ports:
            self.ports[mac] = port
        return True

    def remove(self, mac):
        port = self.ports.pop(mac, None)
        if port is not None:
            self.writers.pop(port, None)
        return True

    def refresh(self):
        ports = [port
                 for port, writer in self.writers.items()
                 if not writer.writeable()]
        while ports:
            self.writers.pop(ports.pop(), None)

    def start_refresh(self, loop, interval=19.0):
        if self.timer is not None:
            self.timer.cancel()
        self.refresh()
        self.timer = loop.call_later(interval, self.start_refresh, loop, interval)

    def close(self):
        if self.timer is not None:
            self.timer.cancel()


class Writer(object):
    def __init__(self, fd, ifname):
        self.fd = fd
        self.ifname = ifname

    def writeto(self, frame, ifname):
        os.write(self.fd, frame)

    def writeable(self):
        return True

    def __str__(self):
        return self.ifname


def _tap_linux(ifname):
    fd = os.open('/dev/net/tun', os.O_RDWR)
    ifreq = struct.pack('<16sH',
                        ifname.encode(), 0x1000 | 0x0002)
    fcntl.ioctl(fd, 0x400454ca, ifreq)
    return fd, ifname


def _tap_darwin(ifname):
    for i in range(128):
        fd = 0
        ifname = 'tap{i}'.format(i=i)
        try:
            fd = os.open(os.path.join('/dev', ifname), os.O_RDWR)
        except OSError as e:
            if 0x10 == e.errno:
                continue
            else:
                raise e
        return fd, ifname
    raise SystemError('No available device')


def tap(ifname):
    impl = _tap_darwin if 'Darwin' == platform.system() else _tap_linux
    return impl(ifname)
